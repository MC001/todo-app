import { createReducer, on } from '@ngrx/store';
import { crear, toogle, editar, eliminar, toggleAll, eliminarCompletados } from './todo.actions';
import { Todo } from './models/todo.model';

export const estadoInicial: Todo[] = [
    new Todo('Salvar al Mundo'),
    new Todo('Vencer a Thanos'),
    new Todo('Comprar traje de Ironman'),
    new Todo('Robar escudo del Capitán América')
];

const _todoReducer = createReducer(estadoInicial,
  on(crear, (state, { texto }) => [...state, new Todo( texto )]),
  on(toogle, (state, { id }) => {
    return state.map( todo => {
      if (todo.id === id) {
        return {
          ...todo,
          completado: !todo.completado
        };
      } else {
        return todo;
      }
    });
  }),
  on(editar, (state, { id, texto }) => {
    return state.map( todo => {
      if (todo.id === id) {
        return {
          ...todo,
          texto
        };
      } else {
        return todo;
      }
    });
  }),
  on(eliminar, (state, { id }) => {
    return state.filter( p => p.id !== id);
  }),
  on(toggleAll, (state, { estado }) => {
    return state.map( todo => {
      return {
        ...todo,
        completado: estado
      };
    });
  }),
  on(eliminarCompletados, (state) => {
    return state.filter( todo => !todo.completado);
  }),
);

export function todoReducer(state, action) {
  return _todoReducer(state, action);
}

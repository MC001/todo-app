import { Todo } from './models/todo.model';
import { ActionReducerMap } from '@ngrx/store';
import { todoReducer } from './todo.reducer';
import { filtrosValidos } from '../filtro/filtro.actions';
import { filtroReducer } from '../filtro/filtro.reducer';

export interface AppState {
    todos: Todo[];
    filtro: filtrosValidos;
}

export const appReducer: ActionReducerMap<AppState> = {
    todos: todoReducer,
    filtro: filtroReducer
};

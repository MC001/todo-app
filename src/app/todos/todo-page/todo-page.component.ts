import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { toggleAll } from '../todo.actions';
import { AppState } from '../app.reducer';

@Component({
  selector: 'app-todo-page',
  templateUrl: './todo-page.component.html',
  styleUrls: ['./todo-page.component.scss']
})
export class TodoPageComponent implements OnInit {
  completados = false;
  constructor( private store: Store<AppState> ) { }

  ngOnInit(): void {
  }

  toggleAll(): void {
    this.completados = !this.completados;
    this.store.dispatch(toggleAll({ estado: this.completados }));
  }
}
